<?php

namespace Drupal\eca_condition\Plugin\Action;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Plugin\Action\ConfigurableActionBase;
use Drupal\eca_condition\Events\ConditionEvent;

/**
 * Action set condition result.
 *
 * @Action(
 *   id = "eca_condition_result",
 *   label = @Translation("ECA Condition: set result"),
 *   description = @Translation("Set result condition")
 * )
 */
class SetConditionResultAction extends ConfigurableActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute(): void {
    $event = $this->getEvent();
    if (!$event || !($event instanceof ConditionEvent)) {
      return;
    }
    $value = $this->configuration['value'];
    $event->setResult($value);
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'value' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['value'] = [
      '#type' => 'select',
      '#required' => FALSE,
      '#title' => $this->t('Condition result'),
      '#default_value' => $this->configuration['value'],
      '#empty_option' => $this->t('- Select -'),
      '#options' => [
        0 => $this->t('False'),
        1 => $this->t('True'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['value'] = $form_state->getValue('value');
    parent::submitConfigurationForm($form, $form_state);
  }

}
