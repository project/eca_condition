<?php

namespace Drupal\eca_condition\Plugin\Condition;

use Drupal\Core\Condition\ConditionPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Event\TriggerEvent;

/**
 * Provides a condition plugin.
 *
 * @Condition(
 *   id = "eca_condition",
 *   label = @Translation("ECA Condition")
 * )
 */
class ECACondition extends ConditionPluginBase {

  /**
   * Conditions key.
   */
  const CONDITIONS = 'eca_conditions';

  /**
   * The service for triggering ECA-related events.
   *
   * @var \Drupal\eca\Event\TriggerEvent
   */
  protected TriggerEvent $triggerEvent;

  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->triggerEvent = \Drupal::service('eca.trigger_event');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      self::CONDITIONS => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate() {
    $conditions = $this->configuration[self::CONDITIONS];
    if (!mb_strlen(trim($conditions))) {
      return FALSE;
    }

    $conditions = array_filter(preg_split("/\r?\n/", $conditions));
    foreach ($conditions as $condition) {
      /** @var \Drupal\eca_condition\Events\ConditionEvent $event */
      $event = $this->triggerEvent->dispatchFromPlugin('eca_condition:condition_event', $condition);
      if (empty($event)) {
        return FALSE;
      }
      $result = $event->getResult();
      if (!$result) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form[self::CONDITIONS] = [
      '#type' => 'textarea',
      '#required' => FALSE,
      '#title' => $this->t('Conditions'),
      '#description' => $this->t('List of ECA condition event'),
      '#cols' => 60,
      '#rows' => 5,
      '#default_value' => $this->configuration[self::CONDITIONS] ?? '',
    ];

    $form = parent::buildConfigurationForm($form, $form_state);
    $form['negate']['#access'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration[self::CONDITIONS] = $form_state->getValue(self::CONDITIONS);
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function summary() {
    return $this->getPluginDefinition()['label'];
  }

}
