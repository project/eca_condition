<?php

namespace Drupal\eca_condition\Plugin\ECA\Event;

use Drupal\Core\Form\FormStateInterface;
use Drupal\eca\Attributes\Token;
use Drupal\eca\Event\Tag;
use Drupal\eca\Plugin\ECA\Event\EventBase;
use Drupal\eca_condition\Events\ConditionEvent;

/**
 * Plugin implementation of the ECA Condition for config.
 *
 * @EcaEvent(
 *   id = "eca_condition",
 *   deriver = "Drupal\eca_condition\Plugin\ECA\Event\ECAConditionEventDeriver"
 * )
 */
class ECAConditionEvent extends EventBase {

  /**
   * {@inheritdoc}
   */
  public static function definitions(): array {
    return [
      'condition_event' => [
        'label' => t('ECA Condition'),
        'event_name' => ConditionEvent::CONDITION_EVENT,
        'event_class' => ConditionEvent::class,
        'tags' => Tag::RUNTIME | Tag::BEFORE,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $values = [
      'condition_id' => '',
    ];
    return $values + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['condition_id'] = $form_state->getValue('condition_id');
  }

  /**
   * {@inheritdoc}
   */
  #[Token(
    name: 'event',
    description: 'The event.',
    classes: [
      ConditionEvent::class,
    ],
    properties: [
      new Token(name: 'condition_id', description: 'The condition id of the event.'),
    ],
  )]
  protected function buildEventData(): array {
    $event = $this->event;
    $data = [];
    if ($event instanceof ConditionEvent) {
      $data += [
        'condition_id' => $event->getConditionId(),
      ];
    }
    $data += parent::buildEventData();
    return $data;
  }

}
