<?php

namespace Drupal\eca_condition\Plugin\ECA\Event;

use Drupal\eca\Plugin\ECA\Event\EventDeriverBase;

/**
 * Deriver for eca_condition event plugins.
 */
class ECAConditionEventDeriver extends EventDeriverBase {

  /**
   * {@inheritdoc}
   */
  protected function definitions(): array {
    return ECAConditionEvent::definitions();
  }

}
