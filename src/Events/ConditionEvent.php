<?php

namespace Drupal\eca_condition\Events;

use Drupal\Component\EventDispatcher\Event;
use Drupal\eca\Event\TokenReceiverInterface;
use Drupal\eca\Event\TokenReceiverTrait;

/**
 * The Condition Event.
 *
 * @package Drupal\eca_condition\Events
 */
class ConditionEvent extends Event implements TokenReceiverInterface {

  use TokenReceiverTrait;

  /**
   * Condition event.
   */
  const CONDITION_EVENT = 'eca_condition.condition_event';

  /**
   * The data.
   */
  protected bool $data;

  /**
   * The key.
   *
   * @var string
   */
  protected string $conditionId;

  /**
   * Additional arguments provided by the triggering context.
   *
   * @var array
   */
  protected array $arguments = [];

  /**
   * Handler for condition result.
   */
  protected bool $result = FALSE;

  /**
   * Eca Condition constructor.
   *
   * @param string $condition_id
   *   The condition id.
   * @param array $arguments
   *   (optional) Additional arguments provided by the triggering context.
   */
  public function __construct(string $condition_id, array $arguments = []) {
    $this->conditionId = $condition_id;
    $this->arguments = $arguments;
  }

  /**
   * Get the condition id of event.
   *
   * @return string
   *   The condition id.
   */
  public function getConditionId(): string {
    return $this->conditionId;
  }

  /**
   * Set condition result.
   *
   * @param bool $value
   *   The value.
   */
  public function setResult(bool $value): void {
    $this->result = $value;
  }

  /**
   * Get condition result.
   *
   * @return bool
   *   The value.
   */
  public function getResult(): bool {
    return $this->result;
  }

}
